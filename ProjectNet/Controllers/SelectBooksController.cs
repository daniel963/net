﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectNet.Models;

namespace ProjectNet.Controllers
{
    public class SelectBooksController : Controller
    {
        // GET: SelectBooks
        BooksContext db = new BooksContext();
        public ActionResult Index()
        {
            List<Books> model = db.Books.ToList();

            return View(model);
        }
    }
}